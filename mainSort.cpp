#include "sort.h"
#include "stopwatch.h"
#include <vector>
#include <iostream>
int main()
{
  Stopwatch *watch = new Stopwatch();
  //  std::vector<int> mylist = {1,2,3,8,4,5,6,7};
  int n;
  double timeConsumed;
  
  std::cin >> n;
  srand((unsigned)time(NULL));
  
  std::vector<int> mylist(n);
  
  for(int i=0;i<n;i++)
    mylist[i]=rand();

  /*
  std::cout<<"[";
  for (int i=0; i< mylist.size() ; i++){
    std::cout << mylist[i] ;
    if(i < mylist.size()-1)
      std::cout << ",";
  }
  std::cout << "]"<< std::endl;
  */
  
  watch->start();  
  mergesort(mylist);
  timeConsumed= watch->stop();

  std::cout << "Time: " << timeConsumed << std::endl;

  /*
  std::cout<<"[";
  for (int i=0; i< mylist.size() ; i++){
    std::cout << mylist[i];
    if(i < mylist.size()-1)
      std::cout << ",";
  }
  std::cout << "]"<< std::endl;
  */
  
  return 0;
}
